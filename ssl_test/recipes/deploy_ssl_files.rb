Chef::Log.info("Entering deploy-ssl-files")

app = search("aws_opsworks_app", "name:#{node[:app_name]}").first
app_ssl = app[:ssl_configuration]

config_dir = app[:environment]['config_location']
pki_dir = File.join(config_dir, 'pki')

[File.dirname(config_dir), config_dir, pki_dir].each do |path|
	directory path do
		owner 'root'
		group 'root'
		mode '0755'
	end
end

file "#{pki_dir}/STAR_sorensonmedia_com.crt" do
	owner "root"
	mode 0400
	content app_ssl[:chain]
end

file "#{pki_dir}/sorensonmedia.key" do
	owner "root"
	mode 0400
	content app_ssl[:private_key]
end

file "#{pki_dir}/client_ca.crt" do
	owner "root"
	mode 0400
	content app_ssl[:certificate]
end